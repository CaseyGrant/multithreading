#include <thread> // allows the use of threads
#include <iostream> // allows the use of input and output

using namespace std; //makes it so you don't have to put std:: everywhere

static int totalRocketsLaunchedIntoSpace; // holds the number of rockets launched

static bool bigRedAbortButton = false; // hold the position of the abort button

void LaunchRocketFromOffsiteRocketPad(); // initializes the function
void TheAfterMath(); // initializes the function

void main() // the main function
{
	std::cout << "Welcome space cadet to the launch program!\n\n"; // text
	std::cout << "The main thread of the launch program is " << this_thread::get_id() << "\n\n"; // text
	system("pause"); // pauses

	int missionControlLaunchedRockets = 0; // holds the rockets launched by mission control

	std::cout << "Launching rocket from mission control...\n\n" << ++missionControlLaunchedRockets << " rocket launched \n\n"; // text
	std::cout << "Total number of rockets launched " << ++totalRocketsLaunchedIntoSpace << "\n\n"; // text

	std::cout << "Starting the offsite thread...\n\n"; // text
	thread startOffsiteRocketLaunchThread(LaunchRocketFromOffsiteRocketPad); // uses multi threading to call a function

	system("pause > nul"); // pauses

	bigRedAbortButton = true; // changes the position of the abort button

	startOffsiteRocketLaunchThread.join(); // joins the thread

	std::cout << "Starting the aftermath thread\n\n"; // text
	thread repercussions(TheAfterMath); // uses multi threading to call a function

	repercussions.join(); // joins the thread
}

void LaunchRocketFromOffsiteRocketPad() // the Launch Rocket From Offsite Rocket Pad function
{
	using namespace::literals::chrono_literals; // allows the use of chrono literals
	
	int offsiteLaunchRockets = 0; // holds the amount of offsite rockets launched

	std::cout << "Launch pad thread " << this_thread::get_id() << "\n\n"; // text
	std::cout << "Press Enter to abort the rockets at any time...\n\n"; // text

	while (bigRedAbortButton != true) //while the abort button is not pressed
	{
		std::cout << "Starting Countdown to next launch...\n\n"; // text
		std::cout << "1\n"; // text
		this_thread::sleep_for(1s); // pauses for one second
		std::cout << "2\n"; // text
		this_thread::sleep_for(1s); // pauses for one second
		std::cout << "3\n"; // text
		this_thread::sleep_for(1s); // pauses for one second
		std::cout << "Launching rocket from an off base launch pad\n\n" << ++offsiteLaunchRockets << " rocket/s launched\n\n"; // text
		std::cout << "Total number of rockets launched " << ++totalRocketsLaunchedIntoSpace << "\n\n"; // text
	}
}

void TheAfterMath() // the after math function
{
	std::cout << "You have pressed the abort button\n\n"; // text
	std::cout << "So the rockets malfunctioned and fell to earth...\n\n"; // text
	std::cout << "Millions have died...\n\n"; // text
	std::cout << "If only you had aborted earlier...\n\n"; // text
	std::cout << "Each rocket killed 5 million people...\n\n"; // text
	std::cout << "Your failure to act has killed " << totalRocketsLaunchedIntoSpace * 5 << " MILLION people\n\n"; // text
}